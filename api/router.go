package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/golanguzb70/middleware/gin/basicauth"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/samandar_tukhtayev/rest/api/docs" // docs
	v1 "gitlab.com/samandar_tukhtayev/rest/api/handlers/v1"
	"gitlab.com/samandar_tukhtayev/rest/config"
	"gitlab.com/samandar_tukhtayev/rest/pkg/logger"
	"gitlab.com/samandar_tukhtayev/rest/storage"
)

// Option ...
type Option struct {
	Conf     config.Config
	Logger   *logger.Logger
	Postgres storage.StorageI
}

// New ...
// @title           Trello project API Endpoints
// @version         1.0
// @description     Here QA can test and frontend or mobile developers can get information of API endpoints.

// @BasePath  /v1

// @securityDefinitions.basic BasicAuth
// @in header
// @name Authorization
func New(log *logger.Logger, cfg config.Config, strg storage.StorageI) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	h := v1.New(&v1.HandlerV1Config{
		Logger:   log,
		Cfg:      cfg,
		Postgres: strg,
	})

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	authConf := basicauth.Config{
		Users: []basicauth.User{
			{
				UserName: cfg.AdminUsername,
				Password: cfg.AdminPassword,
			},
		},
		RequireAuthForAll: true,
	}

	router.Use(basicauth.New(&authConf).Middleware)
	api := router.Group("/v1")

	user := api.Group("/user")
	user.POST("", h.UserCreate)
	user.GET("/:id", h.UserGet)
	user.GET("/list", h.UserFind)
	user.PUT("", h.UserUpdate)
	user.DELETE(":id", h.UserDelete)

	order := api.Group("/order")
	order.POST("", h.OrderCreate)
	order.GET("/:id", h.OrderGet)
	order.GET("/list", h.OrderFind)
	order.PUT("", h.OrderUpdate)
	order.DELETE(":id", h.OrderDelete)

	book := api.Group("/book")
	book.POST("", h.BookCreate)
	book.GET("/:id", h.BookGet)
	book.GET("/list", h.BookFind)
	book.PUT("", h.BookUpdate)
	book.DELETE(":id", h.BookDelete)

	// Don't delete this line, it is used to modify the file automatically

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}
