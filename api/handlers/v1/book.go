package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/samandar_tukhtayev/rest/models"
)

// @Router		/book [POST]
// @Summary		Create book
// @Tags        Book
// @Description	Here book can be created.
// @Accept      json
// @Produce		json
// @Param       post   body       models.BookCreateReq true "post info"
// @Success		200 	{object}  models.BookApiResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) BookCreate(c *gin.Context) {
	body := &models.BookCreateReq{}
	err := c.ShouldBindJSON(&body)
	if HandleBadRequestErrWithMessage(c, h.log, err, "c.ShouldBindJSON(&body)") {
		return
	}

	res, err := h.storage.Postgres().BookCreate(context.Background(), body)
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().BookCreate()") {
		return
	}

	c.JSON(http.StatusOK, &models.BookApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/book/{id} [GET]
// @Summary		Get book by key
// @Tags        Book
// @Description	Here book can be got.
// @Accept      json
// @Produce		json
// @Param       id       path     int true "id"
// @Success		200 	{object}  models.BookApiResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) BookGet(c *gin.Context) {

	res, err := h.storage.Postgres().BookGet(context.Background(), &models.BookGetReq{
		ID: c.Param("id"),
	})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().BookGet()") {
		return
	}

	c.JSON(http.StatusOK, models.BookApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/book/list [GET]
// @Summary		Get books list
// @Tags        Book
// @Description	Here all books can be got.
// @Accept      json
// @Produce		json
// @Param       filters query models.BookFindReq true "filters"
// @Success		200 	{object}  models.BookApiFindResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) BookFind(c *gin.Context) {
	page, err := ParsePageQueryParam(c)
	if HandleBadRequestErrWithMessage(c, h.log, err, "helper.ParsePageQueryParam(c)") {
		return
	}
	limit, err := ParseLimitQueryParam(c)
	if HandleBadRequestErrWithMessage(c, h.log, err, "helper.ParseLimitQueryParam(c)") {
		return
	}

	res, err := h.storage.Postgres().BookFind(context.Background(), &models.BookFindReq{
		Page:  page,
		Limit: limit,
	})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().BookFind()") {
		return
	}

	c.JSON(http.StatusOK, &models.BookApiFindResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Summary		Update book
// @Tags        Book
// @Description	Here book can be updated.
// @Accept      json
// @Produce		json
// @Param       post   body       models.BookUpdateReq true "post info"
// @Success		200 	{object}  models.BookApiResponse
// @Failure     default {object}  models.DefaultResponse
// @Router		/book [PUT]
func (h *handlerV1) BookUpdate(c *gin.Context) {
	body := &models.BookUpdateReq{}
	err := c.ShouldBindJSON(&body)
	if HandleBadRequestErrWithMessage(c, h.log, err, "c.ShouldBindJSON(&body)") {
		return
	}

	res, err := h.storage.Postgres().BookUpdate(context.Background(), body)
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().BookUpdate()") {
		return
	}

	c.JSON(http.StatusOK, &models.BookApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/book/{id} [DELETE]
// @Summary		Delete book
// @Tags        Book
// @Description	Here book can be deleted.
// @Accept      json
// @Produce		json
// @Param       id       path     int true "id"
// @Success		200 	{object}  models.DefaultResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) BookDelete(c *gin.Context) {

	err := h.storage.Postgres().BookDelete(context.Background(), &models.BookDeleteReq{ID: c.Param("id")})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().BookDelete()") {
		return
	}

	c.JSON(http.StatusOK, models.DefaultResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "Successfully deleted",
	})
}
