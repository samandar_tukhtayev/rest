package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/samandar_tukhtayev/rest/config"
	"gitlab.com/samandar_tukhtayev/rest/pkg/logger"
	"gitlab.com/samandar_tukhtayev/rest/storage"
)

type HandlerV1I interface {
	UserCreate(c *gin.Context)
	UserGet(c *gin.Context)
	UserFind(c *gin.Context)
	UserUpdate(c *gin.Context)
	UserDelete(c *gin.Context)

	OrderCreate(c *gin.Context)
	OrderGet(c *gin.Context)
	OrderFind(c *gin.Context)
	OrderUpdate(c *gin.Context)
	OrderDelete(c *gin.Context)

	BookCreate(c *gin.Context)
	BookGet(c *gin.Context)
	BookFind(c *gin.Context)
	BookUpdate(c *gin.Context)
	BookDelete(c *gin.Context)
}

type handlerV1 struct {
	log     *logger.Logger
	cfg     config.Config
	storage storage.StorageI
}

type HandlerV1Config struct {
	Logger   *logger.Logger
	Cfg      config.Config
	Postgres storage.StorageI
}

// New ...
func New(c *HandlerV1Config) HandlerV1I {
	return &handlerV1{
		log:     c.Logger,
		cfg:     c.Cfg,
		storage: c.Postgres,
	}
}
