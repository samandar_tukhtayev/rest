package v1

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/samandar_tukhtayev/rest/models"
)

// @Router		/order [POST]
// @Summary		Create order
// @Tags        Order
// @Description	Here order can be created.
// @Accept      json
// @Produce		json
// @Param       post   body       models.OrderCreateReq true "post info"
// @Success		200 	{object}  models.OrderApiResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) OrderCreate(c *gin.Context) {
	body := &models.OrderCreateReq{}
	err := c.ShouldBindJSON(&body)
	if HandleBadRequestErrWithMessage(c, h.log, err, "c.ShouldBindJSON(&body)") {
		return
	}

	res, err := h.storage.Postgres().OrderCreate(context.Background(), body)
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().OrderCreate()") {
		return
	}

	c.JSON(http.StatusOK, &models.OrderApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/order/{id} [GET]
// @Summary		Get order by key
// @Tags        Order
// @Description	Here order can be got.
// @Accept      json
// @Produce		json
// @Param       id       path     int true "id"
// @Success		200 	{object}  models.OrderApiResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) OrderGet(c *gin.Context) {
	res, err := h.storage.Postgres().OrderGet(context.Background(), &models.OrderGetReq{
		ID: c.Param("id"),
	})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().OrderGet()") {
		return
	}

	c.JSON(http.StatusOK, models.OrderApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/order/list [GET]
// @Summary		Get orders list
// @Tags        Order
// @Description	Here all orders can be got.
// @Accept      json
// @Produce		json
// @Param       filters query models.OrderFindReq true "filters"
// @Success		200 	{object}  models.OrderApiFindResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) OrderFind(c *gin.Context) {
	page, err := ParsePageQueryParam(c)
	if HandleBadRequestErrWithMessage(c, h.log, err, "helper.ParsePageQueryParam(c)") {
		return
	}
	limit, err := ParseLimitQueryParam(c)
	if HandleBadRequestErrWithMessage(c, h.log, err, "helper.ParseLimitQueryParam(c)") {
		return
	}

	res, err := h.storage.Postgres().OrderFind(context.Background(), &models.OrderFindReq{
		Page:  page,
		Limit: limit,
	})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().OrderFind()") {
		return
	}

	c.JSON(http.StatusOK, &models.OrderApiFindResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Summary		Update order
// @Tags        Order
// @Description	Here order can be updated.
// @Accept      json
// @Produce		json
// @Param       post   body       models.OrderUpdateReq true "post info"
// @Success		200 	{object}  models.OrderApiResponse
// @Failure     default {object}  models.DefaultResponse
// @Router		/order [PUT]
func (h *handlerV1) OrderUpdate(c *gin.Context) {
	body := &models.OrderUpdateReq{}
	err := c.ShouldBindJSON(&body)
	if HandleBadRequestErrWithMessage(c, h.log, err, "c.ShouldBindJSON(&body)") {
		return
	}

	res, err := h.storage.Postgres().OrderUpdate(context.Background(), body)
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().OrderUpdate()") {
		return
	}

	c.JSON(http.StatusOK, &models.OrderApiResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "",
		Body:         res,
	})
}

// @Router		/order/{id} [DELETE]
// @Summary		Delete order
// @Tags        Order
// @Description	Here order can be deleted.
// @Accept      json
// @Produce		json
// @Param       id       path     int true "id"
// @Success		200 	{object}  models.DefaultResponse
// @Failure     default {object}  models.DefaultResponse
func (h *handlerV1) OrderDelete(c *gin.Context) {

	err := h.storage.Postgres().OrderDelete(context.Background(), &models.OrderDeleteReq{ID: c.Param("id")})
	if HandleDatabaseLevelWithMessage(c, h.log, err, "h.storage.Postgres().OrderDelete()") {
		return
	}

	c.JSON(http.StatusOK, models.DefaultResponse{
		ErrorCode:    ErrorSuccessCode,
		ErrorMessage: "Successfully deleted",
	})
}
