package main

import "gitlab.com/samandar_tukhtayev/rest/app"

func main() {
	app := app.New()
	app.Run()
}
