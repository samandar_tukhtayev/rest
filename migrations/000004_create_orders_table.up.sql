CREATE TABLE IF NOT EXISTS "orders"(
    "id" UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "admin_id" UUID REFERENCES users(id),
    "student_id" UUID REFERENCES users(id),
    "book_id" UUID REFERENCES books(id),
    "start_time" TIMESTAMP,
    "end_time" TIMESTAMP,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP
);
