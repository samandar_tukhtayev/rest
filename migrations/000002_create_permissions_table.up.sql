CREATE TABLE IF NOT EXISTS "permissions"(
    "id" UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "role" VARCHAR(255) NOT NULL,
    "url" VARCHAR(255) NOT NULL,
    "action" VARCHAR(255) NOT NULL
);


INSERT INTO permissions (role, url, action) VALUES
('admin', '/admin/dashboard', 'POST'),
('admin', '/admin/users', 'edit'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/dashboard', 'view'),
('user', '/profile', 'edit');
