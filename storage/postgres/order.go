package postgres

import (
	"context"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/spf13/cast"
	"gitlab.com/samandar_tukhtayev/rest/models"
)

func (r *postgresRepo) OrderCreate(ctx context.Context, req *models.OrderCreateReq) (*models.OrderResponse, error) {

	res := &models.OrderResponse{}
	query := r.Db.Builder.Insert("orders").Columns(
		"admin_id",
		"book_id",
		"student_id",
		"start_time",
		"end_time",
	).Values(req.AdminID, req.BookID, req.StartTime, req.EndTime).Suffix(
		"RETURNING id, admin_id, book_id, student_id, start_time, end_time, created_at, updated_at")

	err := query.RunWith(r.Db.Db).Scan(
		&res.ID, &res.AdminID,
		&res.BookID, &res.StudentID,
		&res.StartTime, &res.EndTime,
		&CreatedAt, &UpdatedAt,
	)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *TemplateRepo) Create()")
	}
	res.CreatedAt = cast.ToString(CreatedAt)
	res.UpdatedAt = cast.ToString(UpdatedAt)

	return res, nil
}

func (r *postgresRepo) OrderFind(ctx context.Context, req *models.OrderFindReq) (*models.OrderFindResponse, error) {
	var (
		res = &models.OrderFindResponse{}
	)

	countQuery := r.Db.Builder.Select("count(1) as count").From("orders").Where("deleted_at is null")
	err := countQuery.RunWith(r.Db.Db).QueryRow().Scan(&res.Count)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) FindList()")

	}

	query := r.Db.Builder.Select("id, admin_id, book_id, student_id, start_time, end_time, created_at, updated_at").
		From("orders").Where("deleted_at is null").OrderBy("created_at").Limit(uint64(req.Limit)).Offset(uint64((req.Page - 1) * req.Limit))

	rows, err := query.RunWith(r.Db.Db).Query()
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) FindList()")
	}
	defer rows.Close()

	for rows.Next() {
		order := &models.OrderResponse{}
		err := rows.Scan(
			&order.ID, &order.AdminID,
			&order.BookID, &order.StudentID,
			&order.StartTime, &order.EndTime,
			&CreatedAt, &UpdatedAt,
		)
		if err != nil {
			return res, HandleDatabaseError(err, r.Log, "(r *models.OrdersOrderRepo) FindList()")
		}

		order.CreatedAt = cast.ToString(CreatedAt)
		order.UpdatedAt = cast.ToString(UpdatedAt)
		res.Orders = append(res.Orders, order)
	}

	return res, nil
}

func (r *postgresRepo) OrderGet(ctx context.Context, req *models.OrderGetReq) (*models.OrderResponse, error) {
	query := r.Db.Builder.Select("id, admin_id, book_id, student_id, start_time, end_time, created_at, updated_at").
		From("orders").
		Where(squirrel.Eq{"id": req.ID})

	res := &models.OrderResponse{}
	err := query.RunWith(r.Db.Db).QueryRow().Scan(
		&res.ID, &res.AdminID,
		&res.BookID, &res.StudentID,
		&res.StartTime, &res.EndTime,
		&CreatedAt, &UpdatedAt,
	)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *TemplateRepo) Get()")
	}

	res.CreatedAt = cast.ToString(CreatedAt)
	res.UpdatedAt = cast.ToString(UpdatedAt)

	return res, nil
}

func (r *postgresRepo) OrderUpdate(ctx context.Context, req *models.OrderUpdateReq) (*models.OrderResponse, error) {
	mp := make(map[string]interface{})
	mp["admin_id"] = req.AdminID
	mp["book_id"] = req.BookID
	mp["student_id"] = req.StudentID
	mp["start_time"] = req.StartTime
	mp["end_time"] = req.EndTime
	mp["updated_at"] = time.Now()
	query := r.Db.Builder.Update("orders").SetMap(mp).
		Where(squirrel.Eq{"id": req.ID}).
		Suffix("RETURNING id, admin_id, book_id, student_id, start_time, end_time, created_at, updated_at")

	res := &models.OrderResponse{}
	err := query.RunWith(r.Db.Db).QueryRow().Scan(
		&res.ID, &res.AdminID,
		&res.BookID, &res.StudentID,
		&res.StartTime, &res.EndTime,
		&CreatedAt, &UpdatedAt,
	)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *models.OrderTemplateRepo) Update()")
	}
	res.CreatedAt = cast.ToString(CreatedAt)
	res.UpdatedAt = cast.ToString(UpdatedAt)

	return res, nil
}

func (r *postgresRepo) OrderDelete(ctx context.Context, req *models.OrderDeleteReq) error {
	query := r.Db.Builder.Delete("orders").Where(squirrel.Eq{"id": req.ID})

	_, err := query.RunWith(r.Db.Db).Exec()
	return HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) Delete()")
}
