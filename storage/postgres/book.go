package postgres

import (
	"context"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/samandar_tukhtayev/rest/models"
)

func (r *postgresRepo) BookCreate(ctx context.Context, req *models.BookCreateReq) (*models.BookResponse, error) {
	res := &models.BookResponse{}

	query := r.Db.Builder.
		Insert("books").
		Columns("title", "description", "author", "page_count", "count").
		Values(req.Title, req.Description, req.Author, req.PageCount, req.Count).
		Suffix("RETURNING id, title, description, author, page_count, count, created_at, updated_at")

	err := query.RunWith(r.Db.Db).Scan(
		&res.ID, &res.Title, &res.Description, &res.Author, &res.PageCount, &res.Count, &res.CreatedAt, &res.UpdatedAt,
	)
	if err != nil {
		return nil, HandleDatabaseError(err, r.Log, "(r *postgresRepo) BookCreate()")
	}

	return res, nil
}

func (r *postgresRepo) BookFind(ctx context.Context, req *models.BookFindReq) (*models.BookFindResponse, error) {
	var (
		res = &models.BookFindResponse{}
	)

	countQuery := r.Db.Builder.Select("count(1) as count").From("books").Where("deleted_at is null")
	err := countQuery.RunWith(r.Db.Db).QueryRow().Scan(&res.Count)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) FindList()")

	}

	query := r.Db.Builder.Select("id, title, description, author, page_count, count, created_at, updated_at").
		From("books").Where("deleted_at is null").OrderBy("created_at").Limit(uint64(req.Limit)).Offset(uint64((req.Page - 1) * req.Limit))

	rows, err := query.RunWith(r.Db.Db).Query()
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) FindList()")
	}
	defer rows.Close()

	for rows.Next() {
		book := &models.BookResponse{}
		err := rows.Scan(
			&book.ID, &book.Title,
			&book.Description, &book.Author,
			&book.PageCount, &book.Count,
			&CreatedAt, &UpdatedAt,
		)
		if err != nil {
			return res, HandleDatabaseError(err, r.Log, "(r *models.BooksBookRepo) FindList()")
		}

		book.CreatedAt = CreatedAt
		book.UpdatedAt = UpdatedAt
		res.Books = append(res.Books, book)
	}

	return res, nil
}

func (r *postgresRepo) BookGet(ctx context.Context, req *models.BookGetReq) (*models.BookResponse, error) {
	query := r.Db.Builder.Select("id, title, description, author, page_count, count, created_at, updated_at").
		From("books").
		Where(squirrel.Eq{"id": req.ID})

	res := &models.BookResponse{}
	err := query.RunWith(r.Db.Db).QueryRow().Scan(
		&res.ID, &res.Title,
		&res.Description, &res.Author,
		&res.PageCount, &res.Count,
		&CreatedAt, &UpdatedAt,
	)
	if err != nil {
		return res, HandleDatabaseError(err, r.Log, "(r *TemplateRepo) Get()")
	}
	res.CreatedAt = CreatedAt
	res.UpdatedAt = UpdatedAt

	return res, nil
}

func (r *postgresRepo) BookUpdate(ctx context.Context, req *models.BookUpdateReq) (*models.BookResponse, error) {
	query := `
	UPDATE books
	SET title = $1, description = $2, author = $3, page_count = $4, count = $5, updated_at = $6
	WHERE id = $7
	RETURNING id, title, description, author, page_count, count, created_at, updated_at`

	var res models.BookResponse

	var createdAt, updatedAt time.Time

	err := r.Db.Db.QueryRowContext(ctx, query, req.Title, req.Description, req.Author, req.PageCount, req.Count, time.Now(), req.ID).
		Scan(&res.ID, &res.Title, &res.Description, &res.Author, &res.PageCount, &res.Count, &createdAt, &updatedAt)

	if err != nil {
		return &res, HandleDatabaseError(err, r.Log, "(r *models.BookTemplateRepo) Update()")
	}

	res.CreatedAt = createdAt
	res.UpdatedAt = updatedAt

	return &res, nil
}

func (r *postgresRepo) BookDelete(ctx context.Context, req *models.BookDeleteReq) error {
	query := r.Db.Builder.Delete("books").Where(squirrel.Eq{"id": req.ID})

	_, err := query.RunWith(r.Db.Db).Exec()
	return HandleDatabaseError(err, r.Log, "(r *models.TemplateTemplateRepo) Delete()")
}
