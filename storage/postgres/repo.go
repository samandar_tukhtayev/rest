package postgres

import (
	"context"

	"gitlab.com/samandar_tukhtayev/rest/models"
)

type PostgresI interface {
	UserCreate(ctx context.Context, req *models.UserCreateReq) (*models.UserResponse, error)
	UserGet(ctx context.Context, req *models.UserGetReq) (*models.UserResponse, error)
	UserFind(ctx context.Context, req *models.UserFindReq) (*models.UserFindResponse, error)
	UserUpdate(ctx context.Context, req *models.UserUpdateReq) (*models.UserResponse, error)
	UserDelete(ctx context.Context, req *models.UserDeleteReq) error

	OrderCreate(ctx context.Context, req *models.OrderCreateReq) (*models.OrderResponse, error)
	OrderGet(ctx context.Context, req *models.OrderGetReq) (*models.OrderResponse, error)
	OrderFind(ctx context.Context, req *models.OrderFindReq) (*models.OrderFindResponse, error)
	OrderUpdate(ctx context.Context, req *models.OrderUpdateReq) (*models.OrderResponse, error)
	OrderDelete(ctx context.Context, req *models.OrderDeleteReq) error

	BookCreate(ctx context.Context, req *models.BookCreateReq) (*models.BookResponse, error)
	BookGet(ctx context.Context, req *models.BookGetReq) (*models.BookResponse, error)
	BookFind(ctx context.Context, req *models.BookFindReq) (*models.BookFindResponse, error)
	BookUpdate(ctx context.Context, req *models.BookUpdateReq) (*models.BookResponse, error)
	BookDelete(ctx context.Context, req *models.BookDeleteReq) error
}
