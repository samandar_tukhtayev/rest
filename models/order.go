package models

type Order struct {
	ID        string `json:"id"`
	AdminID   string `json:"admin_id"`
	BookID    string `json:"book_id"`
	StudentID string `json:"student_id"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type OrderCreateReq struct {
	AdminID   string `json:"admin_id"`
	BookID    string `json:"book_id"`
	StudentID string `json:"student_id"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type OrderUpdateReq struct {
	ID        string `json:"id"`
	AdminID   string `json:"admin_id"`
	BookID    string `json:"book_id"`
	StudentID string `json:"student_id"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type OrderGetReq struct {
	ID string `json:"id"`
}

type OrderFindReq struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type OrderDeleteReq struct {
	ID string `json:"id"`
}

type OrderFindResponse struct {
	Orders []*OrderResponse `json:"orders"`
	Count  int              `json:"count"`
}

type OrderResponse struct {
	ID        string `json:"id"`
	AdminID   string `json:"admin_id"`
	BookID    string `json:"book_id"`
	StudentID string `json:"student_id"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type OrderApiResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *OrderResponse
}

type OrderApiFindResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *OrderFindResponse
}
