package models

type Permission struct {
	ID     string `json:"id"`
	Role   string `json:"role"`
	Url    string `json:"url"`
	Action string `json:"actions"`
}

type PermissionCreateReq struct {
	Role   string `json:"role"`
	Url    string `json:"url"`
	Action string `json:"actions"`
}

type PermissionUpdateReq struct {
	Id     int    `json:"id"`
	Role   string `json:"role"`
	Url    string `json:"url"`
	Action string `json:"actions"`
}

type PermissionGetReq struct {
	Id int `json:"id"`
}

type PermissionFindReq struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type PermissionDeleteReq struct {
	Id int `json:"id"`
}

type PermissionFindResponse struct {
	Permissions []*PermissionResponse `json:"permissions"`
	Count       int                   `json:"count"`
}

type PermissionResponse struct {
	ID     int64  `json:"id"`
	Role   string `json:"role"`
	Url    string `json:"url"`
	Action string `json:"actions"`
}

type PermissionApiResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *PermissionResponse
}

type PermissionApiFindResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *PermissionFindResponse
}
