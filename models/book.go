package models

import "time"

type Book struct {
	ID          string    `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Author      string    `json:"author"`
	PageCount   int64     `json:"page_count"`
	Count       int64     `json:"count"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type BookCreateReq struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Author      string `json:"author"`
	PageCount   int64  `json:"page_count"`
	Count       int64  `json:"count"`
}

type BookUpdateReq struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Author      string `json:"author"`
	PageCount   int64  `json:"page_count"`
	Count       int64  `json:"count"`
}

type BookGetReq struct {
	ID string `json:"id"`
}

type BookFindReq struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type BookDeleteReq struct {
	ID string `json:"id"`
}

type BookFindResponse struct {
	Books []*BookResponse `json:"books"`
	Count int             `json:"count"`
}

type BookResponse struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Author      string    `json:"author"`
	PageCount   int64     `json:"page_count"`
	Count       int64     `json:"count"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type BookApiResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *BookResponse
}

type BookApiFindResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Body         *BookFindResponse
}
